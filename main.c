#include "parser.hpp"
#include <unistd.h>
#include <map>
#include <vector>
#include <utility>
#include <algorithm>
#include <sstream>
#include <string>

extern int yylex(void);

bool lexer_only = false;

std::map<std::string, std::string> var_map;
std::list<std::string> function_calls;

std::string function_call(std::string& fname, std::list<std::string>& args){
	std::stringstream ss;
	ss << "function_call:" << fname << ", ";
	for(auto it = args.begin(); it != std::prev(args.end()); it++)
		ss << *it << ", ";
	ss << * std::prev(args.end());
	return ss.str();
}

void add_env_to_var_map(char ** envp){
	for (char ** env = envp; *env != NULL; env++){
		std::string env_string{*env};
		size_t equals_pos = env_string.find("=", 0);
		std::string env_name = env_string.substr(0, equals_pos);
		// + 1 on the equals position to not take it in the value
		std::string env_value = env_string.substr(equals_pos + 1, std::string::npos);
		var_map[env_name] = env_value;
	}
}

int main(int argc, char ** argv, char **envp){
	int c;
	bool no_env_vars = false;
	while((c = getopt(argc, argv, "ln")) != -1){
		switch(c){
			case 'l': lexer_only = true;
					  break;
			case 'n': no_env_vars = true;
					  break;
		}
	}
	if(!no_env_vars)
		add_env_to_var_map(envp);	

	if(lexer_only)
		yylex();
	else{
		yyparse();
		typedef std::pair<std::string, std::string> string_pair;
		std::vector<string_pair> sorted(var_map.begin(), var_map.end());
		std::sort(sorted.begin(), sorted.end(), [](string_pair& lhs, string_pair& rhs) {return lhs.first < rhs.first;});
		for (string_pair& item : sorted)
			std::cout << item.first << ":" << item.second << std::endl;
		for (auto& item : function_calls)
			std::cout << item << std::endl;
	}
}
