

program: lexer.c parser.cpp parser.hpp main.c
	g++ $^ -o $@

lexer.c: flex.lex 
	flex -o $@ $^

parser.cpp parser.hpp: bison.y
	bison $^ --defines=parser.hpp -o parser.cpp

LEXER_TESTS:=$(wildcard tests/lexer/*.m)
PARSER_TESTS:=$(wildcard tests/parser/*.m)

.PHONY: tests $(LEXER_TESTS) $(PARSER_TESTS) env-test clean

clean:
	rm parser.cpp parser.hpp lexer.c

tests: program $(LEXER_TESTS) $(PARSER_TESTS) $(PARSER_ENV_TESTS) env-test

$(LEXER_TESTS): %.m: %.correct
	@./program -l < $@ | cmp -s $^ || (echo test $@ failed && echo "input:" && cat $@ && echo "output:" && ./program -l < $@  && echo "expected:" && cat $^ &&  exit 1)
	@echo $@ passed

$(PARSER_TESTS): %.m: %.correct
	@./program -n < $@ | cmp -s $^ || (echo test $@ failed && echo "input:" && cat $@ && echo "output:" && ./program -n < $@  && echo "expected:" && cat $^ &&  exit 1)
	@echo $@ passed

$(PARSER_ENV_TESTS): %.m: %.correct
	@./program -n < $@ | cmp -s $^ || (echo test $@ failed && echo "input:" && cat $@ && echo "output:" && ./program -n < $@  && echo "expected:" && cat $^ &&  exit 1)
	@echo $@ passed

env-test:
	@echo "" | ENVIRONMENT_VAR=XD ./program | egrep -q "ENVIRONMENT_VAR:XD" || (echo "Failed" && echo "output:" && echo "" | ENVIRONMENT_VAR=XD ./program &&  exit 1)
	@echo $@ passed
