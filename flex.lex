%option noyywrap
%option nounput

%{

#define YY_USER_ACTION 															\
	do{																			\
		yylloc.first_line = yylloc.last_line;									\
		yylloc.first_column = yylloc.last_column;								\
		for(int i = 0; i < yyleng; i++){										\
			if(yytext[i] == '\n'){												\
				yylloc.last_line++;												\
				yylloc.last_column = 1;											\
			}																	\
			else																\
				yylloc.last_column++;											\
		}																		\
	}while(0);

#include <cstdio>
#include <string>
#include "parser.hpp"
extern YYSTYPE yylval;
extern bool lexer_only;
extern YYLTYPE yylloc;
%}

word	[-a-zA-Z0-9_\./:]+

%x COMMENT

%%

call						if(lexer_only) printf("CALL\n"); else return CALL;

eval						if(lexer_only) printf("EVAL\n"); else return EVAL;

ifeq						if(lexer_only) printf("IFEQ\n"); else return IFEQ;

ifneq						if(lexer_only) printf("IFNEQ\n"); else return IFNEQ;

ifndef						if(lexer_only) printf("IFNDEF\n"); else return IFNDEF;

ifdef						if(lexer_only) printf("IFDEF\n"); else return IFDEF;

error						if(lexer_only) printf("ERROR\n"); else return ERROR;

endif						if(lexer_only) printf("ENDIF\n"); else return ENDIF;

^[[:space:]]*\n				;

\#[^\n]*\n					;

{word}                      {
								if(lexer_only)
									printf("word: %s\n", yytext);
								else{
									yylval.string = new std::string(yytext);
									return WORD;
								}
							}

\$                          if(lexer_only) printf("DOLLAR\n"); else return '$';

\(                          if(lexer_only) printf("LPAR\n"); else return '(';

\)                          if(lexer_only) printf("RPAR\n"); else return ')';

,							if(lexer_only) printf("COMMA\n"); else return ',';

\\\n                        ; /* continue because it's a logical line */

\n                          if(lexer_only) printf("ENDLINE\n"); else return ENDLINE;

=                           if(lexer_only) printf("ASSIGNMENT\n"); else return '=';

\+=                         if(lexer_only) printf("APPENDS\n"); else return APPENDS;

.                           ;
%%


