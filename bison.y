

%{
	#include <map>
	#include <string>
	#include <list>
	extern int yylex(void);
	extern std::map<std::string, std::string> var_map;
	extern std::list<std::string> function_calls;
	void yyerror(const char * s);
	extern std::string function_call(std::string& fname, std::list<std::string>& args);
%}


%locations
%token-table
%code requires
{
	#include <string>
	#include <list>
	#include <iostream>
	#include <map>
}

%define parse.error verbose

%union{
	std::string * string;
	std::list<std::string> * stringList;
}

%token APPENDS "+="
%token ASSIGNMENT '='

%token CALL "call"
%token EVAL "eval"
%token IFEQ "ifeq"
%token IFNEQ "ifneq"
%token ENDIF "endif"
%token ERROR "error"
%token IFNDEF "ifndef"
%token IFDEF "ifdef"
%token ENDLINE

%token <string> WORD

%type <string> evaluation
%type <string> evaluation_repetition
%type <stringList> arg_comma_repetition
%%

Makefile:
	statement_repetition
;

conditional:
	"ifneq" comparison statement_repetition "endif"
	| "ifeq" comparison statement_repetition "endif"
	| "ifndef" WORD ENDLINE statement_repetition "endif"
	| "ifdef" WORD ENDLINE statement_repetition "endif"
;
comparison:
	'(' evaluation ',' evaluation ')' ENDLINE
;
statement_repetition:
	statement ENDLINE statement_repetition
	| %empty
;

statement:
	variable_assignment
	| variable_append
	| function_call
	| eval_call
	| conditional
	| error_call
;

error_call:
     '$' '(' "error" evaluation_repetition ')'

variable_assignment:
	evaluation_repetition '=' evaluation_repetition {var_map[*$1] = *$3; free($1); free($3);}
;

variable_append:
	evaluation_repetition "+=" evaluation_repetition {var_map[*$1] += ":" + *$3;}
;

evaluation:
	'$' '(' WORD ')' {$$ = new std::string(var_map[*$3]);}
	// | function_call {printf("TODO"); $$ = new std::string();} Don't need that right now
	| WORD {$$ = $1;}
;

function_call:
	'$' '(' "call" evaluation arg_comma_repetition ')' {function_calls.push_back(function_call(*$4, *$5)); free($5);}


eval_call:
	'$' '(' "eval" function_call ')' {}

evaluation_repetition:
	evaluation evaluation_repetition {$$ = new std::string(*$1 + *$2); free($1); free($2);}
	| %empty {$$ = new std::string();}
;

arg_comma_repetition:
	',' evaluation_repetition arg_comma_repetition {$$ = $3; $$ -> push_front(std::string(*$2)); free($2);}
	| %empty {$$ = new std::list<std::string> ;}
;

%%



void yyerror(const char *s)
{
	std::cerr << yylloc.first_line << ":" << yylloc.first_column << ":" << s << std::endl;
	exit(-1);
}
